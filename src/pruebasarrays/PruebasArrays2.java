package pruebasarrays;

import java.util.Arrays;

public class PruebasArrays2 {

    public static void main(String[] args) {

//       1.	Investiga qué es la clase Arrays.
//2.	¿En qué librería está?
//En clase PruebasArrays2
//3.	Importa la clase Arrays de java.
//4.	Declara y crea un array de números decimales.
        double decimales[] = {22, 66, 3.5, 1, 8, 6.7, 8, 33};
//5.	¿Cuáles son los métodos que se pueden aplicar a tu variable de tipo array? 

//6.	¿Cuáles son los métodos estáticos de la clase Arrays que puedo aplicar a mi array pasándolo como argumento?
//7.	Encuentra un método que:
//a.	Devuelva el contenido del array.
        System.out.println(Arrays.toString(decimales));

//b.	Ordena los elementos del array atendiendo a criterios según el tipo de dato
        Arrays.sort(decimales);
        System.out.println(Arrays.toString(decimales));
//c.	Rellena una lista o array copiando el mismo valor en todos los elementos de larray o lista. Útil para reiniciar una lista o array.

        Arrays.fill(decimales, 6);
        System.out.println(Arrays.toString(decimales));

//d.	Permite realizar búsquedas rápidas en una lista o array ordenados. Es necesario que la lista o array estén ordenados, si no lo están, la búsqueda no tendrá éxito.
        double decimales2[] = {22, 66, 3.5, 1, 8, 6.7, 8, 33};
        System.out.println("Indice donde se encuentra el elemento 66:  "
                + Arrays.binarySearch(decimales2, 66));
        Arrays.sort(decimales2);
        System.out.println(Arrays.toString(decimales2));
        System.out.println("");
        System.out.println("Indice donde se encuentra el elemento 66:  "
                + Arrays.binarySearch(decimales2, 66));
//8.	Ponlos a prueba
//9.	Realiza las mismas operaciones creando un array de cadenas y un array de objetos. ¿Cuáles son los criterios de ordenación?
        String cadenas[] = {"Casa", "casa", "cosa"};

//Devuelva el contenido del array.
        System.out.println(Arrays.toString(cadenas));

//b.	Ordena los elementos del array atendiendo a criterios según el tipo de dato
        Arrays.sort(cadenas);
        System.out.println(Arrays.toString(cadenas));

        //d. Busqueda
        System.out.println("Indice donde se encuentra casa: " + Arrays.binarySearch(cadenas, "casa"));

//c.	Rellena una lista o array copiando el mismo valor en todos los elementos de larray o lista. Útil para reiniciar una lista o array.
        Arrays.fill(cadenas, "KKKKK");
        System.out.println(Arrays.toString(cadenas));


        Persona personas[] = new Persona[3];
        Persona p1 = new Persona("Aaaaa", 22, 1.60);
        Persona p2 = new Persona("Bbbbb", 33, 1.70);
        Persona p3 = new Persona("Ccccc", 44, 1.80);

        personas[0] = p1;
        personas[1] = p2;
        personas[2] = p3;

        System.out.println(Arrays.toString(personas));
        // Ordena los elementos del array atendiendo a criterios según el tipo de dato
        // Arrays.sort(personas);
        System.out.println(Arrays.toString(personas));

        //d. Busqueda
        //     System.out.println("Indice donde se encuentra p3: "+Arrays.binarySearch(personas, p3));
        //c.	Rellena una lista o array copiando el mismo valor en todos los elementos de larray o lista. Útil para reiniciar una lista o array.
        Arrays.fill(personas, p1);
        System.out.println(Arrays.toString(personas));
//16.	¿Limitaciones de la clase Arrays?
    }

}
