
package pruebasarrays;

public class PruebasArrays1 {

    public static void main(String[] args) {
        
//        String nombres [];
//        nombres = new String[5];
        
        
        String nombres[]= {"Laura","ana", "Olivia"};
        
        int arrayNumeros[];
        arrayNumeros = new int[8];
        
         arrayNumeros[0]=9;
         arrayNumeros[1]=3;
         arrayNumeros[2]=2;
         arrayNumeros[3]=33;
         arrayNumeros[4]=1;
         arrayNumeros[5]=15;
         arrayNumeros[6]=26;
         arrayNumeros[7]=44;
         
//        System.out.println ( "elemento en el índice 0:"  + arrayNumeros [0]);
//        System.out.println ( "elemento en el índice 1: " + arrayNumeros [1]);
//        System.out.println ( "elemento en el índice 2:" + arrayNumeros [2]);
//        System.out.println ( "elemento en el índice 3:" + arrayNumeros [3]);
//        System.out.println ( "elemento en el índice 4:"+arrayNumeros [4]);
//        System.out.println ( "elemento en el índice 5: " + arrayNumeros [5]);
//        System.out.println ( "elemento en el índice 6:" + arrayNumeros [6]);
//        System.out.println ( "elemento en el índice 7:" + arrayNumeros [7]);
        
        for (int i = 0; i < arrayNumeros.length; i++) {
            System.out.println("elemento en el índice" + i +":"+arrayNumeros [i] );
        }
        
       
       // System.out.println ( "elemento en el índice 8:"+arrayNumeros [8]);
        
        System.out.println(arrayNumeros.length);
        
        int numeros[]=arrayNumeros.clone();
        
        for (int i = 0; i < numeros.length; i++) {
            System.out.println("elemento en el índice" + i +":"+numeros [i] );
        }
        //numeros[] y arrayNumeros[]  tienen el mismo contenido pero NO son el mismo objeto
        
        System.out.println("Son iguales?"+arrayNumeros.equals(numeros)) ;
        
        
//        arrayNumeros.getClass();
//        arrayNumeros.hashCode();
//        arrayNumeros.notify();
//        arrayNumeros.notifyAll();
        System.out.println(  arrayNumeros.toString());
        
        
        Persona personas[]=new Persona[3];
        Persona p1=new Persona("Aaaaa", 22,1.60);
        Persona p2=new Persona("Bbbbb",33,1.70);
        Persona p3=new Persona("Ccccc",44,1.80);
        
        
        personas[0]=p1;
        personas[1]=p2;
        personas[2]=p3;
        
        System.out.println(personas.toString());
        
        
        
    }
    
}
